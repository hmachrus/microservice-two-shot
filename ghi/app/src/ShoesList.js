import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom";

function ShoesList({shoes, getShoes}) {
    const deleteShoe = async (shoe) => {
      const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`
      const fetchConfig = {
        method: 'delete'
      };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
        getShoes();

    }
    }


    return (
        <>
            <table className="table table-striped table-hover align-middle mt-5">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sample Image</th>
                        <th>Bin</th>
                        <th>Delete Shoes</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map((shoe) => {
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.manufacturer}</td>
                                <td><img src={shoe.picture_url} className="img-thumbnail shoes" width="50px" height="50px" /></td>
                                <td>{shoe.bin.closet_name}</td>
                                <td>
                                    <button type="button" className="btn btn-outline-danger btn-sm" title="Banish to the void!!!!" id={shoe.id} onClick={() => deleteShoe(shoe)}>Delete!</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default ShoesList;

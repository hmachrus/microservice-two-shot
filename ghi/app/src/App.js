import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import HatList from './Hats'
import NewHatForm from './NewHatForm'
import { useEffect, useState } from 'react';

function App(props) {
  const [shoes, setShoes] = useState ([])
  const getShoes = async () => {
    const url = 'http://localhost:8080/api/shoes/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json();
      const shoes = data.shoes
      setShoes(shoes)
      console.log(shoes)
    }
  }
  useEffect(() => {
    getShoes();
  }, []

  )

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList shoes={shoes} getShoes={getShoes} />} />
          <Route path="shoes/new" element={<ShoesForm shoes={shoes} getShoes={getShoes} />} />
            <Route path="hats" element={<HatList hats={props.hats} />} />
            <Route path="createhat" element={<NewHatForm />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

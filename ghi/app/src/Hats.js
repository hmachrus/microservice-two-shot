import React from 'react';
import './index.css';
import DeleteHat from './Deletehat';

function HatList(props) {
	return (
		<table className="table table-striped">
			<thead>
				<tr>
					<th>Style Name</th>
					<th>Fabric</th>
					<th>Color</th>
					<th>Location</th>
					<th>Prodcut Preview</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{props.hats.map((hat) => {
					return (
						<tr key={hat.id}>
							<td>{hat.style_name}</td>
							<td>{hat.fabric}</td>
							<td>{hat.color}</td>
							<td>{hat.location}</td>
							<td>
								{' '}
								<img
									className="hatsize"
									src={hat.picture_url}
								/>{' '}
							</td>
							<td>
								<DeleteHat id={hat.id} />{' '}
							</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

export default HatList;

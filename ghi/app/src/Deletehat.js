import React from 'react';
import './index.css';

function DeleteHat(props) {
	const handleSubmit = async (event) => {
		const locationUrl = `http://localhost:8090/api/hats/${props.id}`;
		const fetchConfig = {
			method: 'DELETE',
			body: JSON.stringify(props.id),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		try {
			const response = await fetch(locationUrl, fetchConfig);
		} catch (error) {
			console.log(error);
		}
	};
	return (
		<button className="DeleteButton" onClick={handleSubmit}>
			Delete Hat
		</button>
	);
}

export default DeleteHat;

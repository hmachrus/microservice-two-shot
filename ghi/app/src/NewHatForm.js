import React, {useEffect, useState} from 'react';

function NewHatForm() {

    const [style_name, setStyle_name] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const [picture_url, setPicture_url] = useState('');





    const handleStyle_nameChange = (event) => {
        const value = event.target.value;
        setStyle_name(value);
      }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
      }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
      }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
      }
    const handlePicture_urlChange = (event) => {
        const value = event.target.value;
        setPicture_url(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.style_name = style_name;
        data.fabric = fabric;
        data.color = color;
        data.location = location;
        data.picture_url = picture_url;

        console.log(data);

        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        try{

            const response = await fetch(locationUrl, fetchConfig);
            setStyle_name('');
            setFabric('');
            setColor('');
            setLocation('');
            setPicture_url('');
          }
        catch(error) {
            console.log(error)
          }





      }





    const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);


    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)


    }
  }

  useEffect(() => {
    fetchData();
  }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={style_name} onChange={handleStyle_nameChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
              <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a Location</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.closet_name}
                    </option>
                    );
                })}
              </select>
              </div>
              <div className="form-floating mb-3">
                <input value={picture_url} onChange={handlePicture_urlChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default NewHatForm;

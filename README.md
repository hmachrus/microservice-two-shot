# Wardrobify

Team:

* Dean Davidson - Hats microservice
* Hana Machrus - Shoes microservice

## Design

## Shoes microservice

###### Models:
`binVO`: (Bin value object) Contains information about each bin including the closet_name, bin_number, and bin_size, and import_href.

`Shoes`: Includes the model_name, manufacturer, color, bin location, and picture_url for each (pair of) shoe(s). Foreign key is used in this model to link shoes to `binVO` and allow them to be "placed" into certain bins.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

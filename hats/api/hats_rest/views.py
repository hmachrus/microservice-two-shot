from django.shortcuts import render
from .models import Hats, LocationVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
    ]
    encoders = {"location": LocationVOEncoder}

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}



@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,

        )
    else:
        content = json.loads(request.body)
        try:
            id = content["location"]
            location = LocationVO.objects.get(id=id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location ID"},
                status=400
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def show_hat(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse(
        {"deleted": count > 0}
    )

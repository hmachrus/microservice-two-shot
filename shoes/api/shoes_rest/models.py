from django.db import models
from django.urls import reverse

# Create your models here.

class binVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=100, unique=True)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name

class Shoes(models.Model):
    model_name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        binVO,
        related_name = "shoes",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.model_name

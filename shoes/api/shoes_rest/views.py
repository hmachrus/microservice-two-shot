from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoes, binVO
# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = binVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
        ]

class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "id",
        "manufacturer",
        "color",
        "picture_url",

    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders = {"bin": BinVOEncoder(),}



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            import_href = content["bin"]
            bin = binVO.objects.get(import_href=import_href)
            content["bin"] = bin
        except binVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin ID"},
                status=400
            )

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoes(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
